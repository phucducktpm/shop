<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//
Route::get('language/{language}', 'LanguageController@change')->name('language');
// Route::get('auth/facebook', 'Web\LoginController@redirectToFacebook');
// Route::get('auth/facebook/callback', 'Web\LoginController@handleFacebookCallback');
// --- Route web page
Route::group(['namespace' => 'Web'], function () {
    // login social
    Route::get('auth/{provider}', 'AuthController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
    // Auth
    Route::get('register', 'AuthController@create')->name('auth');
    Route::post('register', 'AuthController@store')->name('auth');
    Route::get('login/facebook', 'AuthController@redirectToFacebook');
    Route::get('login/facebook/callback', 'AuthController@handleFacebookCallback');
    // Dashboard
    Route::get('home', 'HomeController@index')->name('home');

    // Review
    Route::get('review', 'ReviewController@create')->name('review');
    Route::post('review', 'ReviewController@store')->name('review');

    // Contact
    Route::get('contact', 'ContactController@create')->name('contact');
    Route::post('contact', 'ContactController@store')->name('contact');

    // address-province
    Route::prefix('address-province')->group(function () {
        Route::get('/', 'AddressProvinceController@index');
        Route::get('/{id}', 'AddressProvinceController@show');
    });

    // address-district
    Route::prefix('address-district')->group(function () {
        Route::get('/', 'AddressDistrictController@index');
        Route::get('/{id}', 'AddressDistrictController@show');
    });

    // address-ward
    Route::prefix('address-ward')->group(function () {
        Route::get('/', 'AddressWardController@index');
        Route::get('/{id}', 'AddressWardController@show');
    });
});

// --- Route cms page
Route::group(['namespace' => 'Cms'], function () {
    Route::prefix('cms')->group(function () {
        // Auth
        Route::get('/register', 'AuthController@getRegister')->name('cms-register');
        Route::post('/register', 'AuthController@postRegister')->name('cms-register');
        Route::get('/login', 'AuthController@getLogin')->name('cms-login');
        Route::post('/login', 'AuthController@postLogin')->name('cms-login');
        Route::get('/reset-pass', 'AuthController@getResetPass')->name('cms-reset-pass');
        Route::post('/reset-pass', 'AuthController@postResetPass')->name('cms-reset-pass');

        Route::group(['middleware' => 'auth:web'], function () {
            Route::get('/dashboard', 'DashboardController@index')->name('cms-dashboard');
            // product
            Route::prefix('product')->group(function () {
                Route::get('/list', 'ProductController@getProducts')->name('cms-products');
                Route::get('/listdataproduct', 'ProductController@getDataProducts')->name('cms-data-products');
                Route::get('/add', 'ProductController@getAdd')->name('cms-product-add');
                Route::post('/add', 'ProductController@postAdd')->name('cms-product-add');
                // Route::get('/edit/{id}', 'ProductController@show')->name('cms-products');
                // Route::put('/edit/{id}', 'Cms\UserController@update');

            });

            //payment
            Route::prefix('payment')->group(function () {
                Route::get('/stripe', 'PaymentController@viewFormStripe')->name('cms-payment-stripe');
                Route::get('/ocb', 'PaymentController@viewFormOCB')->name('cms-payment-ocb');
                Route::get('/vcb', 'PaymentController@viewFormVCB')->name('cms-payment-vcb');
                //Route::get('/listdataproduct', 'ProductController@getDataProducts')->name('cms-data-products');
                //Route::get('/add', 'ProductController@getAdd')->name('cms-product-add');
                //Route::post('/add', 'ProductController@postAdd')->name('cms-product-add');
                // Route::get('/edit/{id}', 'ProductController@show')->name('cms-products');
                // Route::put('/edit/{id}', 'Cms\UserController@update');

            });
        });
    });
});
