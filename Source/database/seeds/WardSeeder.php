<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Model\AddressWard;
class WardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->importWards();
    }

    function importWards(){
        $wards = file_get_contents(__DIR__ . '/data/wards.json');
        $wardsDecode = json_decode($wards);
        DB::table('address_wards')->delete();

        foreach ($wardsDecode as $ward) {
            $data = AddressWard::create([
                'id' => $ward->id,
                'name' => $ward->name,
                'district_id' => $ward->district_id,
            ]);
            echo "{$data}\n";
        }
        echo "\n*** DONE Wards! ***\n";
    }
}
