<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Model\AddressProvince;
class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->importProvinces();
    }

    function importProvinces()
    {
        $provinces = file_get_contents(__DIR__ . '/data/provinces.json');
        $provincesDecode = json_decode($provinces);
        DB::table('address_provinces')->delete();

        foreach ($provincesDecode as $province) {
            $data = AddressProvince::create([
                'id' => $province->id,
                'name' => $province->name,
            ]);
            echo "{$data}\n";
        }
        echo "\n*** DONE Provinces! ***\n";
    }
}
