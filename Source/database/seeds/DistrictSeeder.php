<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Model\AddressDistrict;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->importDistricts();
    }

    function importDistricts()
    {
        $districts = file_get_contents(__DIR__ . '/data/districts.json');
        $districtsDecode = json_decode($districts);
        DB::table('address_districts')->delete();

        foreach ($districtsDecode as $district) {
            $data = AddressDistrict::create([
                'id' => $district->id,
                'name' => $district->name,
                'province_id' => $district->province_id,
            ]);
            echo "{$data}\n";
        }
        echo "\n*** DONE Districts! ***\n";
    }

}
