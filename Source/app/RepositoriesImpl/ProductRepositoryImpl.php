<?php

namespace App\RepositoriesImpl;

use Illuminate\Support\Facades\Storage;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Helpers\Traits\UploadImageTrait;

class ProductRepositoryImpl implements ProductRepository
{
   use UploadImageTrait;
   function store($request)
   {
      $data = $request->validated();
      $product = Product::create($data);
    //  dd($product->id);
      if ($request->hasFile('images')) {
         $images = $request->file('images');
         $path = $this->uploadImage($images);
         $product->images()->create([
            'url' => $path,
            'imageable_id' => $product->id,
         ]);
      }
      return $product;
   }

   function getDataProducts($request)
   {
      $limit = $request->get('limit', 10);
      $product = Product::query()->paginate($limit);
      return $product;
   }
}
