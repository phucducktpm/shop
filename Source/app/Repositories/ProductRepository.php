<?php
namespace App\Repositories;
use Illuminate\Http\Request;


interface ProductRepository 
{
    function store($request);
    function getDataProducts($request);
}