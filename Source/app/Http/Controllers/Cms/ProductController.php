<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Http\Requests\Cms\Product\StoreProductRequest;
use App\Model\Product;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {   
        $this->middleware('auth');
        $this->productRepository = $productRepository;
    }

    public function getProducts()
    {

        return view('cms.product.list');
    }

    public function getDataProducts(Request $request)
    {
        
        $product = $this->productRepository->getDataProducts($request);
        return $product;
    }

    public function getAdd()
    {
        return view('cms.product.add');
    }

    public function postAdd(StoreProductRequest $request)
    {
        $product = $this->productRepository->store($request);
        return $this->responseSuccess($product);
    }
}
