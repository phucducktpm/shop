<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PaymentRepository;
class PaymentController extends Controller
{
    //
    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository)
    {   
        $this->middleware('auth');
        $this->paymentRepository = $paymentRepository;
    }

    public function viewFormStripe()
    {
        return view('cms.payment.stripe');
    }
    public function viewFormOCB()
    {
        return view('cms.payment.ocb');
    }
    public function viewFormVCB()
    {
        return view('cms.payment.vcb');
    }
}
