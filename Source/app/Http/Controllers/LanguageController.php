<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function change(Request $request, $language)
    {
       if($language){
            $request->session()->put('language', $language);
       }
       return redirect()->back();
    }
}
