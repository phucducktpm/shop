<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    static function responseSuccess($data, $message = "", $code = 200)
    {
        return response()->json([
            "code" => $code,
            "message" => $message,
            "data" => $data
        ]);
    }

    static function responseError($message ="", $data = "", $code = 500)
    {
        return response()->json([
            "code" => $code,
            "message" => $message,
            "data" => $data
        ]);
    }
}
