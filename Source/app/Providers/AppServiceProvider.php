<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\ProductRepository;
use App\RepositoriesImpl\ProductRepositoryImpl;
use App\Repositories\PaymentRepository;
use App\RepositoriesImpl\PaymentRepositoryImpl;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ProductRepository::class, function () {
            return new ProductRepositoryImpl();
        });
        $this->app->singleton(PaymentRepository::class, function () {
            return new PaymentRepositoryImpl();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
