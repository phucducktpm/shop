<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table = 'images';

    protected $fillable = [
        'id',
        'url',
        'alt',
        'imageable_id',
        'imageable_type',
        'created_at',
        'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'imageable_id', 'imageable_type');
    }


}
