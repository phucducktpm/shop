<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'products';

    protected $fillable = [
        'id',
        'name',
        'description',
        'brand_id',
        'purchase_price',
        'sale_price',
        'status',
        'created_at',
        'updated_at'
    ];
    public function brand()
    {
        return $this->hasOne(Brand::class,'brand_id');
    } 

    public function images()
    {
        return $this->hasMany(Image::class,'imageable_id','imageable_type');
    }

}
