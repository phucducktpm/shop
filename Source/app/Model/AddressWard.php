<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddressWard extends Model
{
    protected $table = 'address_wards';

    protected $fillable = [
        'id',
        'name',
        'district_id'
    ];
}
