<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddressDistrict extends Model
{
    protected $table = 'address_districts';

    protected $fillable = [
        'id',
        'name',
        'province_id'
    ];

    public function addressWards()
    {
        return $this->hasMany(AddressWard::class, 'district_id');
    }
}
