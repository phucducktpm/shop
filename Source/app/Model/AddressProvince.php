<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddressProvince extends Model
{
    protected $table = 'address_provinces';

    protected $fillable = [
        'id',
        'name',
    ];

    public function addressDistricts()
    {
        return $this->hasMany(AddressDistrict::class, 'province_id');
    }
}
