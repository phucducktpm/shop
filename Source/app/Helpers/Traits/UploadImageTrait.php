<?php
namespace App\Helpers\Traits;

use Illuminate\Http\UploadedFile;
use App\Helpers\Status\PathConstants;
use Illuminate\Support\Str;
trait UploadImageTrait
{
    public function uploadImage($image)
    {
        $name = Str::random(7) . time() . $image->getClientOriginalName();
        $folder = PathConstants::PathUploadImage;
        $filePath = $folder . $name;
        $this->uploadOne($image, $folder, 'public', $name);
        return $filePath;
    }

    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);
        $file = $uploadedFile->storeAs($folder, $name, $disk);  
        return $file;
    }

    


}