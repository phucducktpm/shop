<?php

// resources/lang/en/messages.php

return [
    'welcome'   => 'Welcome to our application',
    'language' => 'Language',
    'account' => 'Account',
    'login' => 'Login',
    'search' => 'Search',
    'add_new'   => 'Add New',
    'cancel'    => 'Cancel',
    'category'  => 'Category',
    'create' => 'Create',
    'created_at' => 'Created At',
    'date'      => 'Date',
    'end_date' => 'End Date',
    'month'     => 'Month',
    'you_are_not_login' => 'You are not login !',
    'start_date' => 'Start Date',
    'updated_at' => 'Updated At',
    // screen register
    'shoptitle' => 'DucShop',
    'create_a_new_account' => 'Create a new Account',
    'year' => '2020',
    'footet_title' => 'DucShop',
    'footer_name' => 'DucNguyen',
    'dashboard' => 'Bashboard',
    // product
    'table_product_list' => 'Table Product List',
    'product' => [
        'product' => 'Product',
        'list' => 'Product List',
        'add' => 'Product Add New',
        'edit' => 'Product Edit',
        'name' => 'Name',
        'brand' => 'Brand',
        'description' => 'Description',
        'purchase_price'      => 'Purchase Price',
        'sale_price'      => 'Sale Price',
        'status'      => 'Status',



        'id'      => 'Product ID',
       
        'code'      => 'Customer Code',
        'address' => 'Address',
        'country' => 'Country',
        'phone' => 'Phone',
        'manage_side' => 'Manage Side',
        'person_in_charge' => 'Person In Charge',
        'pic_mail' => 'PIC Mail',
        'relation_from' => 'Relation From',
        'sales_person' => 'Sales Person',
        'sales_pic' => 'Sales PIC',
        'status' => 'Status',
    ],

];
