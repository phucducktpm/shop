<?php

// resources/lang/vi/messages.php
return [
    'welcome' => 'Xin chào',
    'language' => 'Ngôn Ngữ',
    'account' => 'Tài Khoản',
    'login' => 'Đăng Nhập',
    'search' => "Tìm kiếm",
    'add_new'   => 'Thêm mới',
    'cancel'    => 'Hủy',
    'category'  => 'Phân loại',
    'create' => "Tạo",
    'created_at' => "Ngày tạo",
    'date'      => 'Ngày',
    'end_date' => "Ngày kết thúc",
    'month'     => 'Tháng',
    'you_are_not_login' => "Bạn Chưa Đăng Nhập",
    'start_date' => "Ngày bắt đầu",
    'updated_at' => "Ngày cập nhật",
    // screen register
    'shoptitle' => "DucShop",
    'create_a_new_account' => "Đăng kí tài khoản mới",
    'year' => "2020",
    'footet_title' => "DucShop",
    'footer_name' => "DucNguyen",
    'dashboard' => 'Bashboard',
    // product
    'table_product_list' => 'Bảng Danh Sách Sản Phẩm',
    'product' => [
        '' => 'Sản Phẩm',
        'list' => 'Danh Sách Sản Phẩm',
        'add' => 'Thêm Sản Phẩm',
        'edit' => 'Cập Nhật Sản Phẩm',
        'id'      => 'Mã Sản Phẩm',
        'name' => 'Tên Sản Phẩm',
        'brand' => 'Thương Hiệu',
        'description' => 'Mô Tả',
        'purchase_price'      => 'Giá Nhập',
        'sale_price'      => 'Giá Bán',
        'status'      => 'Trạng Thái',


       
        'code'      => 'Customer Code',
        'address' => 'Address',
        'country' => 'Country',
        'phone' => 'Phone',
        'manage_side' => 'Manage Side',
        'person_in_charge' => 'Person In Charge',
        'pic_mail' => 'PIC Mail',
        'relation_from' => 'Relation From',
        'sales_person' => 'Sales Person',
        'sales_pic' => 'Sales PIC',
        'status' => 'Status',
    ],
];
