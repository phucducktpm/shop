@extends('web.layouts.web')

@section('title', 'Duc Shop | Home')

@push('page-styles')
@endpush

<!-- //write content page -->
@section('content')

@include('web.home.slider')

<section>
    <div class="container">
        <div class="row">
            @include('web.home.content-left')
            @include('web.home.content-right')
        </div>
    </div>
</section>

@endsection

@push('page-scripts')
@endpush