<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- // custom title page -->
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{asset('web/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/price-range.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/prettyPhoto.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/bootstrap.min.css')}}">

    <!-- // import library file css -->
    @stack('page-styles')

</head>
<body>
    <!-- // import file layouts/header-->
    @include('web.layouts.header')

    <!--// write code -->
    @yield('content')

    <!-- // import file layouts/header-->
    @include('web.layouts.footer')

    <script src="{{asset('web/js/jquery.js')}}"></script>
    <script src="{{asset('web/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('web/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('web/js/price-range.js')}}"></script>
    <script src="{{asset('web/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('web/js/main.js')}}"></script>

    <!-- // import library file js -->
    @stack('page-scripts')

</body>
</html>