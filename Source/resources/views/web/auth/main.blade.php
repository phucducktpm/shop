@extends('web.layouts.web')

@section('title', 'Duc Shop | Auth')

@push('page-styles')
@endpush

<!-- //write content page -->
@section('content')

<section id="form">
    <div class="container">
        <div class="row">
            @include('web.auth.login')
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            @include('web.auth.register')
        </div>
    </div>

    
    
    <div class="container">
        <div class="row">
            <div class="col-md-12 row-block">
                <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/facebook') }}">
                    <strong>Login With Facebook</strong>
                </a>
            </div>
        </div>
    </div>
</section>

@endsection

@push('page-scripts')
@endpush