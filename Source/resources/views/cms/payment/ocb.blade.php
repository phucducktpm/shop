<!--------------- Product New ------------>
@extends('cms.layouts.app')
@section('title', 'Add Product | DucShop')

@push('page-styles')
<link href="{{asset('cms/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css">
@endpush

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ __('messages.product.add') }}</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <h1>Payment Ngan Hang Ocb</h1>
            <!-- end row -->
        </div>
    </div>
</div>
@endsection

@push('page-scripts')
<script src="{{asset('cms/libs/dropzone/dropzone.min.js')}}"></script>
@endpush