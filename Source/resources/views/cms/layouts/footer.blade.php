<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            {{ __('messages.year') }} &copy; {{ __('messages.footet_title') }} by <a href="">{{ __('messages.footer_name') }}</a>
            </div>
        </div>
    </div>
</footer>