<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="theme-color" content="#007bff" />
    <meta name="msapplication-navbutton-color" content="#007bff">
    <meta name="apple-mobile-web-app-status-bar-style" content="#007bff">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <link rel="icon" href="{{asset('img/favicon.ico')}}" sizes="16x16"> -->
    <link href="{{asset('cms/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('cms/css/icons.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('cms/css/app.min.css')}}" rel="stylesheet" type="text/css">
    
    <link href="{{asset('custom_style/shop.css')}}" rel="stylesheet" type="text/css">

    @stack('page-styles')
</head>

<body>
    <div id="wrapper">
        @include('cms.layouts.navbar')
        @include('cms.layouts.sidebar')
        @yield('content')
        @include('cms.layouts.footer')
    </div>
    @include('cms.layouts.rightbar')

    <script src="{{asset('cms/js/vendor.min.js')}}"></script>
    <script src="{{asset('cms/js/app.min.js')}}"></script>
    @stack('page-scripts')
</body>

</html>