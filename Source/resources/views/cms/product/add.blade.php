<!--------------- Product New ------------>
@extends('cms.layouts.app')
@section('title', 'Add Product | DucShop')

@push('page-styles')
<link href="{{asset('cms/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css">
@endpush

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ __('messages.product.add') }}</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- <h4 class="header-title mb-4">Input Sizes & Group</h4> -->
                            <form action="{{ route('cms-product-add') }}" method="POST" action="/file-upload" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name" class="col-form-label">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                        @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="brand_id" class="col-form-label">Brand</label>
                                        <select id="brand_id" class="form-control" name="brand_id">
                                            <option></option>
                                            <option value="1">Nike</option>
                                            <option value="2">Adidas</option>
                                            <option value="3">Gucci</option>
                                        </select>
                                        @error('brand_id')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="purchase_price" class="col-form-label">Purchase Price</label>
                                        <input type="number" class="form-control" id="purchase_price" name="purchase_price" placeholder="Purchase Price">
                                        @error('purchase_price')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="sale_price" class="col-form-label">Sale Price</label>
                                        <input type="number" class="form-control" id="sale_price" name="sale_price" placeholder="Sale Price">
                                        @error('sale_price')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="col-form-label">Description</label>
                                    <textarea class="form-control" rows="5" id="description" name="description" placeholder="Description"></textarea>
                                    @error('description')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input name="images" type="file" multiple="">
                                </div>

                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="status" id="status" value="1">Immediately Salling
                                        </label>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
    </div>
</div>
@endsection

@push('page-scripts')
<script src="{{asset('cms/libs/dropzone/dropzone.min.js')}}"></script>
@endpush