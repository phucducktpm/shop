<!--------------- Product List ------------>
@extends('cms.layouts.app')
@section('title', 'List Product | DucShop')

@push('page-styles')

<link href="http://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- <link href="{{asset('cms/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"> -->
<!-- <link href="{{asset('cms/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cms/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cms/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css"> -->
@endpush

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">{{ __('messages.product.list') }}</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <h4 class="m-t-0 header-title mb-4"><b>{{ __('messages.table_product_list') }}</b></h4>
                            <table id="table_product_list" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>{{ __('messages.product.name') }}</th>
                                        <th>{{ __('messages.product.brand') }}</th>
                                        <th>{{ __('messages.product.description') }}</th>
                                        <th>{{ __('messages.product.purchase_price') }}</th>
                                        <th>{{ __('messages.product.sale_price') }}</th>
                                        <th>{{ __('messages.product.status') }}</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>
    </div>
</div>
@endsection

@push('page-scripts')
<script src="http://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<!-- <script src="{{asset('cms/libs/datatables/jquery.dataTables.min.js')}}"></script> -->
<!-- <script src="{{asset('cms/libs/datatables/dataTables.bootstrap4.min.js')}}"></script> -->
<!-- Buttons examples -->
<!-- <script src="{{asset('cms/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('cms/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('cms/libs/jszip/jszip.min.js')}}"></script>
<script src="{{asset('cms/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('cms/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('cms/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('cms/libs/datatables/buttons.print.min.js')}}"></script> -->

<!-- Responsive examples -->
<!-- <script src="{{asset('cms/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('cms/libs/datatables/responsive.bootstrap4.min.js')}}"></script> -->

<!-- <script src="{{asset('cms/libs/datatables/dataTables.keyTable.min.js')}}"></script> -->
<!-- <script src="{{asset('cms/libs/datatables/dataTables.select.min.js')}}"></script> -->

<!-- Datatables init -->
<!-- <script src="{{asset('cms/js/pages/datatables.init.js')}}"></script> -->
<script>
    $(document).ready(function() {
        $('#table_product_list').DataTable({
            "processing": true,
            "pageLength": 50,
            "ajax": {
                "url": "{{route('cms-data-products')}}",
                "type": 'GET',
            },
            "columns": [{
                    "data": "name"
                },
                {
                    "data": "brand_id"
                },
                {
                    "data": "description"
                },
                {
                    "data": "purchase_price"
                },
                {
                    "data": "sale_price"
                },
                {
                    "data": "status",
                    "render":function(data,type,row){
                        if(data == 1) return '<span>active</span>';
                        else return '<span>unactive</span>';
                    }
                },
                {
                    "data": "id",
                    "render": function(data, type, row) {
                        edit_href = "{{url('editproject')}}/" + data;
                        return '<span><a href=' + edit_href + '><button type="button" class="btn btn-outline-primary btn-rounded waves-effect width-md waves-light custom-btn-list-product">Edit</button></a></span>';
                    }
                },
                {
                    "data": "id",
                    "render": function(data, type, row) {
                        delete_href = ' onclick = deleteProject(' + data + ') ';
                        return '<span><a href=' + delete_href + '><button type="button" class="btn btn-outline-danger btn-rounded waves-effect width-md waves-light custom-btn-list-product">Delete</button></a></span>';
                    }
                },
            ],
            "searching": true,
            "ordering": true,
            "info": true,
            "lengthChange": true,
            "orderCellsTop": true,
            "fixedHeader": true,
            "select": true
        });
    });
</script>

@endpush